#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <regex.h>
#include <sys/types.h>
#include <alloca.h>
#include <cmime.h>
#include <errno.h>
#include <b64/cdecode.h>

#include "message.h"
#include "util.h"
#include "config.h"
#include "savegame.h"

regex_t datere, dispre;

void makeregex(void)
{
	int error;
	if(error = regcomp(&datere, "Received: [^;]*;[[:space:]]*([A-Za-z]{3}, )?([0-9]+ [A-Za-z]{3} [0-9]+ [0-9][0-9]:[0-9][0-9]:[0-9][0-9]) ([-+][0-9]{4})", REG_EXTENDED))
	{
		char errmsg[80];
		regerror(error, &datere, errmsg, 80);
		die("Time regex error: %s\n", errmsg);
	}
	if(error = regcomp(&dispre, " attachment; filename=\"(.*\\.asg)\"", REG_EXTENDED | REG_ICASE))
	{
		char errmsg[80];
		regerror(error, &dispre, errmsg, 80);
		die("Content disposition regex error: %s\n", errmsg);
	}
}

time_t received_time(char *message)
{
	int regerr;
	regmatch_t matches[4];

	if((regerr = regexec(&datere, message, 4, matches, 0)) == 0)
	{
		char *datetime, *zone;
		zone = (char *) xmalloc(6);
		datetime = (char *) xmalloc(matches[2].rm_eo - matches[2].rm_so +2);
		strncpy(datetime, message + matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so + 1);
		strncpy(zone, message + matches[3].rm_so, 5);

		DEBUG("Parsing datetime '%s' with zone '%s'.\n", datetime, zone);

		struct tm time; time_t stamp;
		strptime(datetime, "%d %b %Y %H:%M:%S", &time);
		time.tm_isdst = -1;
		stamp = mktime(&time);
		stamp -= decode_zone(zone);

		free(datetime);
		free(zone);

		return stamp;
	}
	else if(regerr == REG_NOMATCH)
	{
		DEBUG("No match!!!!\n");
		return -1;
	}
	else
	{
		char errmsg[80];
		regerror(regerr, &datere, errmsg, 80);
		die("Matching time regex failed badly: %s\n", errmsg);
	}
}

void destroy_lfs(char *string)
{
	int len = strlen(string);
	for(int i = 0; i<len; i++)
		if(string[i] == '\n' || string[i] == '\r')
		{
			memmove(string + i, string + i + 1, len - i + 1);
			i--;
		}
}

size_t unbase64(char *in, char **out)
{
	char *rv = (char *) xmalloc(strlen(in));
	base64_decodestate state;
	base64_init_decodestate(&state);

	size_t len = base64_decode_block(in, strlen(in), rv, &state);
	*out = rv;

	return len;
}

char *filename_from_disp(char *disposition)
{
	int regerr;
	regmatch_t matches[2];
	if((regerr = regexec(&dispre, disposition, 2, matches, 0)) == 0)
	{
		char *filename = (char *) xmalloc(262);
		strncpy(filename, disposition + matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so + 1);
		destroy_lfs(filename);

		return filename;
	}
	else return NULL;
}

int handle_message(char *msg)
{
	int successes = 0;
	DEBUG("PWD: '%s'\n", conf->programdir);
	CMimeMessage_T *mime = cmime_message_new();
	int error = cmime_message_from_string(&mime, msg, 0);
	if(error == 1)
	{
		SAY("Bad message, skipping.\n");
		return 0;
	}
	else if(error == 2)
		die("Cannot parse messages because no more memory can be allocated.\n");
	else if(error != 0)
	{
		SAY("Generic error while parsing message.\n");
		return 0;
	}


	CMimePart_T *part;
	for(CMimeListElem_T *elem = cmime_list_head(mime->parts); elem != NULL; elem = elem->next)
	{
		part = (CMimePart_T *) cmime_list_data(elem);
		char *disp = cmime_part_get_content_disposition(part);
		if(disp==NULL) continue;

		char *filename;
		if((filename = filename_from_disp(disp)) != NULL)
		{
			char *save;
			size_t len = unbase64(part->content, &save);

			DEBUG("\tGame File: '%s'\n", filename);
			
			struct gameinfo *stats = parse_savegame(save, len);
			if(stats == NULL) continue;
			DEBUG("\t\t%15s: %s\n\t\t%15s: %s\n\t\t%15s: %d\n%15s:\n", "Game Name", stats->gamename, "Map Name", stats->mapname, "Turn", stats->turn, "Player Addresses");
			for(int i = 0; i<stats->players; i++)
				DEBUG("\t\t\t[%d] Hero '%s' @ '%s'\n", i, stats->playerdata[i].heroname, stats->playerdata[i].mail);

//			archive_append( ... );
			
			char *path = (char *) xmalloc(400);
			snprintf(path, 400, "%s/%s", conf->programdir, stats->gamename);
			if(!exists(path))
				if(mkdir(path, 0744) == -1)
					die("Cannot make directory '%s'.\n", path);

			strcat(path, "/last_turn");
			if(exists(path))
			{
				FILE *f;
				if(f = fopen(path, "r"))
				{
					int turn;
					fscanf(f, "%d", &turn);
					fclose(f);
					if(stats->turn <= turn) continue;
				}
			}

			FILE *f;
			if(f = fopen(path, "w"))
			{
				fprintf(f, "%d", stats->turn);
				fclose(f);
			}
			else die("Cannot write 'last_turn' file: '%s'.\n", strerror(errno));
			snprintf(path, 400, "%s/%s", conf->emailin, filename);

			DEBUG("\tWriting game file to '%s'...\n", path);
			FILE *game;
			if(game = fopen(path, "w"))
			{
				fwrite(save, len, 1, game);
				fclose(game);
			}
			else die("Cannot write game to EmailIn: '%s'.\n", strerror(errno));

			successes++;
			destroy_gameinfo(stats); free(save); free(filename); free(path);
		}
	}
	
	cmime_message_free(mime);
	return successes;
}

void tell_message(char *data)
{
	CMimeMessage_T *mime = cmime_message_new();
	int error = cmime_message_from_string(&mime, data, 0);
	if(error == 1)
	{
		SAY("Bad message, skipping.\n");
		return;
	}
	else if(error == 2)
		die("Cannot parse messages because no more memory can be allocated.\n");
	else if(error != 0)
	{
		SAY("Generic error while parsing message.\n");
		return;
	}


	CMimePart_T *part;
	for(CMimeListElem_T *elem = cmime_list_head(mime->parts); elem != NULL; elem = elem->next)
	{
		part = (CMimePart_T *) cmime_list_data(elem);
		char *disp = cmime_part_get_content_disposition(part);
		if(disp==NULL) continue;

		char *filename;
		if((filename = filename_from_disp(disp)) != NULL)
		{
			char *send = cmime_message_get_sender_string(mime);
			DEBUG("From: '%s'\n", send);
			free(send);
			
			DEBUG("To:\n");
			for(CMimeListElem_T *rec = cmime_list_head(mime->recipients); rec != NULL; rec = rec->next)
				DEBUG("\t * '%s'\n", cmime_string_strip(cmime_address_to_string((CMimeAddress_T *) cmime_list_data(rec))));
			DEBUG("\n");

			char *save;
			size_t len = unbase64(part->content, &save);

			DEBUG("\tGame File: '%s'\n", filename);
			
			struct gameinfo *stats = parse_savegame(save, len);
			if(stats == NULL) continue;
			DEBUG("\t\t%15s: %s\n\t\t%15s: %s\n\t\t%15s: %d\n%15s:\n", "Game Name", stats->gamename, "Map Name", stats->mapname, "Turn", stats->turn, "Player Addresses");
			for(int i = 0; i<stats->players; i++)
				DEBUG("\t\t\t[%d] Hero '%s' @ '%s'\n", i, stats->playerdata[i].heroname, stats->playerdata[i].mail);

			destroy_gameinfo(stats); free(save); free(filename);
		}
	}
	
	cmime_message_free(mime);
}
