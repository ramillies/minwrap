#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "listen.h"
#include "config.h"
#include "util.h"
#include "server_socket.h"
#include "message.h"

#define STATIC_RESPONSE(text) server_write(newfd, text "\r\n", strlen(text) + 2); DEBUG("Wrote '%s'\n", text);
#define SLURP for(bzero(buf, len = 1024), ptr = 0, state = server_read(newfd, buf, &len); state == 0 && server_canread(newfd); ptr += len, len = 1024, state = server_read(newfd, buf + ptr, &len)); DEBUG("Game says: '%s'\n", buf);
static int smtpfd;

void smtp_decoy_make(void)
{
	smtpfd = server_bind(conf->port);
	server_listen(smtpfd, 5);
}

void smtp_decoy_poll(void)
{
	DEBUG("Polling.\n");
	if(!server_canread(smtpfd))
		return;
	
	int newfd = server_accept(smtpfd);
	if(newfd == -1)
	{
		SAY("Cannot accept: %s\n", strerror(errno));
		return;
	}

	char *buf = (char *) xmalloc(1024);
	size_t mailsize = 1048576;
	int len = 1024;
	int ptr, state;

	DEBUG("Prepared to do SMTP.\n");

	STATIC_RESPONSE("220 I wait")
	SLURP
	STATIC_RESPONSE("250 Okay.")
	SLURP
	STATIC_RESPONSE("250 Okay.")
	SLURP
	STATIC_RESPONSE("250 Okay.")
	SLURP
	STATIC_RESPONSE("354 Tell me.")
	char *mail = (char *) xmalloc(mailsize);

	for(ptr = 0, state = server_read(newfd, mail, &len); state == 0 && server_canread(newfd); ptr += len, len = 4096, state = server_read(newfd, mail + ptr, &len))
		if((mailsize-ptr)<8192)
			mail = (char *) xrealloc(mail, mailsize *= 2);

	if(state == -1)
	{
		SAY("Message receiving failed: %s\n", strerror(errno));
		return;
	}

	STATIC_RESPONSE("250 Accepted.")
	SLURP
	STATIC_RESPONSE("221 Good Bye.")

	tell_message(mail);
	free(mail);
	free(buf);
	server_close(newfd);
}
