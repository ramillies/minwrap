#ifndef MESSAGE_H
#define MESSAGE_H

#include <time.h>

void makeregex(void);
time_t received_time(char *);
int handle_message(char *);
void tell_message(char *);
#endif
