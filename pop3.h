#ifndef POP3_H
#define POP3_H

#include <stdlib.h>

struct numsize
{
	int num; size_t size;
};

struct numsize *pop3_stat(void);
struct numsize *pop3_list(int);
struct numsize *pop3_listall(void);
char *pop3_retr(int);
char *pop3_headers(int);
void pop3_dele(int);

#endif
