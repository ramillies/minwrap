#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <zlib.h>

#include "savegame.h"
#include "util.h"
#include "config.h"

#define ASGBYTE(x) ( *((unsigned char *) (asg + (x))) )
#define ASGINT(x) ( *((int32_t *) (asg + (x))) )

char *asg; size_t len;

int isasg(char *data)
{
	return !(memcmp(data, "CFS\0\x02", 5));
}

int32_t sec(int32_t start, int key)
{
	if(start<0) return -1;
	int smallopt = 0, bigopt = 0;
	smallopt = ASGBYTE(start);
	if(smallopt>128)
	{
		smallopt-=128;
		bigopt = ASGINT(start + 1);
	}
	int32_t ptr = start + 1 + (bigopt>0)*4, rel = ptr + 2*smallopt + 8*bigopt;
	for(int i = 0; i<smallopt; i++)
	{
		if(ASGBYTE(ptr + 2*i) == key)
			return ASGBYTE(ptr + 2*i + 1) + rel;
	}
	for(int i = 0; i<bigopt; i++)
	{
		if(ASGINT(ptr + 2*smallopt + 8*i) == key)
			return ASGINT(ptr + 2*smallopt + 8*i + 4) + rel;
	}
	return -1;
}

int secchoices(int32_t start)
{
	if(start<0) return -1;
	int smallopt = 0, bigopt = 0;
	smallopt = ASGBYTE(start);
	if(smallopt>128)
	{
		smallopt-=128;
		bigopt = ASGINT(start + 1);
	}
	return smallopt + bigopt;	
}

int32_t secskip(int32_t start, int key)
{
	return sec(start+4, key);
}

char *emit_pascal(int32_t start)
{
	if(start<0 || start>=len) return NULL;
	char *rv; rv = (char *) xmalloc(ASGBYTE(start)+1);
	strncpy(rv, asg + start + 1, ASGBYTE(start));
	return rv;
}

int emit_int(int32_t start)
{
	if(start<0 || start>=len) return -1;
	return ASGINT(start);
}

struct gameinfo *parse_savegame(char *data, size_t datalen)
{
	struct gameinfo *rv; rv = (struct gameinfo *) xmalloc(sizeof(struct gameinfo));

	if(!isasg(data))
	{
		free(rv);
		return NULL;
	}

	long out = 10*datalen, error = 0;
	asg = (char *) xmalloc(out);

	DEBUG("Before uncompress.\n");
	while((error = uncompress(asg, &out, data + 5, datalen)) != Z_OK)
	{
		if(error == Z_BUF_ERROR)
		{
			out *= 2;
			asg = (char *) xrealloc(asg, out);
		}
		else if(error == Z_MEM_ERROR) die("zlib decompression failed due to insufficient memory.");
		else return NULL;
	}
	
	len = out;

	DEBUG("Got through uncompress.\n");
	DEBUG("First 12 bytes: %s\n", hexdump(asg, 12));
	if(memcmp(asg, "\x10\0\0\0HSM\0\0\0\0\0", 12))
	{
		free(rv);
		free(asg);
		return NULL;
	}

	rv->mapname = emit_pascal(sec(19, 0x19));
	rv->turn = emit_int(sec(19, 0x13));
	rv->gamename = emit_pascal(sec(sec(secskip(sec(sec(sec(19, 0x15), 1), 1), 0x36), 0x15), 0x14));

	rv->players = secchoices(sec(sec(19, 0x15), 1)) - 1;
	rv->playerdata = (struct playerinfo *) xmalloc(rv->players * sizeof(struct playerinfo));
	for(int i = 0; i<rv->players; i++)
	{
		rv->playerdata[i].mail = emit_pascal(sec(sec(secskip(sec(sec(sec(19, 0x15), 1), i+1), 0x36), 0x15), 0x17));
		rv->playerdata[i].heroname = emit_pascal(sec(secskip(sec(sec(sec(19, 0x15), 1), i+1), 0x1E), 0x0A));
	}

	free(asg);
	DEBUG("Got through.\n");
	return rv;
}

void destroy_gameinfo(struct gameinfo *destroyee)
{
	for(int i = 0; i<destroyee->players; i++)
	{
		free(destroyee->playerdata[i].mail);
		free(destroyee->playerdata[i].heroname);
	}
	free(destroyee->playerdata);
	free(destroyee->gamename);
	free(destroyee->mapname);
	free(destroyee);
}
