#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <ctype.h>

#include "config.h"
#include "lcfg_static.h"
#include "util.h"

void parser_die(const char *msg, struct lcfg *parser)
{
	fprintf(stderr, "lcfg fails: %s (lcfg says '%s')\n", msg, lcfg_error_get(parser));
	exit(1);
}

void die(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	exit(1);
}

void *xmalloc(size_t count)
{
	void *ptr;
	if((ptr = malloc(count)) == NULL) die("Cannot allocate memory.\n");
	bzero(ptr, count);
	return ptr;
}

void *xrealloc(void *ptr, size_t count)
{
	void *rv;
	if((rv = realloc(ptr, count)) == NULL) die("Cannot realloc memory.\n");
	return rv;
}

int isdir(const char *path)
{
	struct stat info;
	return !((stat(path, &info) == -1 && errno == ENOENT) || !(S_ISDIR(info.st_mode)));
}

int exists(const char *path)
{
	struct stat info;
	return !(stat(path, &info) == -1 && errno == ENOENT);
}

void daemonize(void)
{
	if(fork())
		exit(0);
	DEBUG("Daemonized with PID %d.\n", getpid());
	setsid();
	chdir("/");
	fclose(stdin);
	fclose(stdout);
	fclose(stderr);

}

char *hexdump(char *data, int len)
{
	char *o = (char *) xmalloc(3*len+1);
	for(int i = 0; i<len; i++)
		sprintf(o+3*i, "%02hhX ", *((unsigned char *) data + i));
	o[3*len-1] = 0;
	return o;
}

char *textdump(char *text, int len)
{
	char *o = (char *) xmalloc(4*len+1);
	int ptr = 0;
	for(int i = 0; i<len; i++)
		if(isprint(*((unsigned char *) text + i)))
			*(o + ptr++) = *((unsigned char *) text + i);
		else
			sprintf(o - 4 + (ptr+=4), "\\x%02hhx", *((unsigned char *) text + i));
	return o;
}

time_t decode_zone(char *zone)
{
	return (zone[0] == '-' ? -1 : 1) * ((zone[1]-'0')*36000 + (zone[2]-'0')*3600 + (zone[3]-'0')*600 + (zone[4]-'0')*60);
}

time_t current_zone(void)
{
	time_t now = time(NULL);
	char zone[6];
	strftime(zone, 6, "%z", localtime(&now));
	return decode_zone(zone);
}
