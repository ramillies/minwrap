#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <alloca.h>
#include <errno.h>
#include <termios.h>
#include <pwd.h>

#include "config.h"
#include "util.h"
#include "lcfg_static.h"
#include "mail.h"
#include "listen.h"

int main(int argc, char **argv)
{
	struct stat info;
	struct passwd *pwdent = getpwuid(getuid());
	char *dir;
	dir = (char *) alloca(400);
	snprintf(dir, 400, "%s/.minwrap", pwdent->pw_dir);
	init_config(2, dir);
	if(!isdir(dir))
		if(mkdir(dir, 0744) == -1)
			die("minwrap's configuration directory ~/.minwrap does not exist and could not create it. Please fix this unappealing condition");


	strcat(dir, "/minwrap.conf");
	if(!exists(dir))
		die("minwrap needs a configuration file. Please make one and place it in ~/.minwrap/minwrap.conf.");
	
	VERBOSE("Using config file '%s'.\n", dir);

	struct lcfg *c;
	c = lcfg_new(dir);
	fill_config(c);

	if(conf->user == NULL)
	{
		if(isatty(fileno(stdin)))
			printf("(minwrap) mail user name: ");
		conf->user = (char *) xmalloc(256);
		fgets(conf->user, 256, stdin);
		CHOMP(conf->user);
	}
	else
		SAY("(minwrap) mail user name: %s\n", conf->user);
	
	struct termios *flags;
	if(conf->pass == NULL)
	{
		if(isatty(fileno(stdin)))
		{
			printf("(minwrap) mail password: ");
			flags = (struct termios *) xmalloc(sizeof(struct termios));
			tcgetattr(fileno(stdin), flags);
			flags->c_lflag &= ~ECHO;
			tcsetattr(fileno(stdin), TCSANOW, flags);
		}
		conf->pass = (char *) xmalloc(256);
		fgets(conf->pass, 256, stdin);
		CHOMP(conf->pass);
		if(isatty(fileno(stdin)))
		{
			flags->c_lflag |= ECHO;
			tcsetattr(fileno(stdin), TCSANOW, flags);
			free(flags);
			putchar(10);
		}
	}

	VERBOSE("%15s: %s\n%15s: %s\n%15s: %s\n%15s: %s\n%15s: %s\n%15s: %s\n%15s: %s\n", "EmailIn", conf->emailin, "EmailOut", conf->emailout, "In Mail Server", conf->mailserver, "Mail Protocol", PROTOSTRING(conf->proto), "Out Mail Server", conf->smtpserver, "Mail Username", conf->user, "Mail Password", "(* * *)");
	DEBUG("Testing mail connections.");

	initrecv();
	testsmtp();
	
	/*
	DEBUG("Check POP:");
	checkpop();
	*/

	smtp_decoy_make();
	while(1)
	{
		smtp_decoy_poll();
		sleep(2);
	}
	return 0;
}
