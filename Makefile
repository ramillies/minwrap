CFILES := config.c util.c lcfg_static.c mail.c curlio.c pop3.c message.c savegame.c server_socket.c listen.c
HFILES := $(CFILES:.c=.h)

CFLAGS := -g -std=c99 -D_POSIX_SOURCE -D_XOPEN_SOURCE
LFLAGS := -lcurl -lz -lcmime -lb64

conftest: $(CFILES) $(HFILES) main.c
	gcc -oconftest $(CFLAGS) main.c $(CFILES) $(LFLAGS)
clean:
	rm -v test

.PHONY: clean
