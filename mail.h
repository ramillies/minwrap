#ifndef MAIL_H
#define MAIL_H

#include <stdlib.h>
#include <curl/curl.h>

#define REQANDBODY(rq, body) curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, rq); curl_easy_setopt(curl, CURLOPT_NOBODY, body);

#define REQUEST(rq) REQANDBODY(rq, 0)
#define NCREQUEST(rq) REQANDBODY(rq, 1)

extern CURL *curl;

void curldie(const char *, CURLcode);
void initrecv(void);
void testsmtp(void);
void destroy_recv(void);

void checkpop(void);

#endif
