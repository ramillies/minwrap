#ifndef SAVEGAME_H
#define SAVEGAME_H

struct playerinfo
{
	char *heroname, *mail;
};

struct gameinfo
{
	char *gamename, *mapname; int turn, players; 
	struct playerinfo *playerdata;
};

struct gameinfo *parse_savegame(char *, size_t);
int isasg(char *data);
void destroy_gameinfo(struct gameinfo *);
#endif
