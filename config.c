#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "config.h"
#include "lcfg_static.h"
#include "util.h"

#define STRING_DEFAULT(key,dest,defaultval) if(lcfg_value_get(parser, # key, (void **) &(dest), &len) == lcfg_status_error) dest = defaultval;
#define STRING_DIE(key,dest,...) if(lcfg_value_get(parser, # key, (void **) &(dest), &len) == lcfg_status_error) die(__VA_ARGS__);
#define INT_DEFAULT(key,dest,defaultval) if(lcfg_value_get(parser, # key, (void **) &num, &len) == lcfg_status_ok) dest = atoi(num); else dest = defaultval;

struct WConfig *conf;

void init_config(int logging, char *pwd)
{
	conf = (struct WConfig *) xmalloc(sizeof(struct WConfig));
	conf->logging = logging;
	conf->programdir = (char *) xmalloc(strlen(pwd)+1);
	strcpy(conf->programdir, pwd);
	fprintf(stderr, "conf->programdir is '%s'\n", conf->programdir);
}

void fill_config(struct lcfg *parser)
{
	if(lcfg_parse(parser) != lcfg_status_ok) parser_die("cannot parse", parser);
	
	char *aow, *num; size_t len;

	if(lcfg_value_get(parser, "aowdir", (void **) &aow, &len) == lcfg_status_ok)
	{
		conf->emailin = (char *) xmalloc(len+8);
		strcpy(conf->emailin, aow); strcat(conf->emailin, "/EmailIn");
		conf->emailout = (char *) xmalloc(len+9);
		strcpy(conf->emailout, aow); strcat(conf->emailout, "/EmailOut");
	}
	else 
		die("Your config file contains no 'aowdir' key. You must assign this key a string with absolute path to the AoW game. It is needed for determining EmailIn/Out directories.");
	
	if(!isdir(conf->emailin)) die("Your 'aowdir' does not contain an EmailIn directory. Please choose the game's true root directory (or make yourself an EmailIn directory in it).");
	if(!isdir(conf->emailout)) die("Your 'aowdir' does not contain an EmailOut directory. Please choose the game's true root directory (or make yourself an EmailOut directory in it).");

	STRING_DIE(mailserver, conf->mailserver, "Your config file contains no 'mailserver' key. Go assign it a meaningful value, then try again.\n");
	STRING_DIE(smtpserver, conf->smtpserver, "Your config file contains no 'smtpserver' key. Go assign it a meaningful value, then try again.\n");
	
	char *proto;
	if(lcfg_value_get(parser, "protocol", (void **) &proto, &len) == lcfg_status_error)
		conf->proto = POP3;
	else
		conf->proto = (strcmp(proto, "IMAP") ? POP3 : IMAP);
	
	STRING_DEFAULT(user, conf->user, NULL);
	STRING_DEFAULT(pass, conf->pass, NULL);
	STRING_DEFAULT(from, conf->from, conf->user);
	INT_DEFAULT(pop_minsize, conf->pop_minsize, 40960);
	INT_DEFAULT(mercy, conf->mercy, 0);
	INT_DEFAULT(sleep, conf->sleep, 15*60);
	INT_DEFAULT(logging, conf->logging, 2);
	INT_DEFAULT(listenport, conf->port, 32768);
}
