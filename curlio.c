#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>

#include "config.h"
#include "util.h"
#include "mail.h"
#include "curlio.h"

struct readbuffer *mkreadbuf(size_t size)
{
	struct readbuffer *rv; rv = (struct readbuffer *) xmalloc(sizeof(struct readbuffer));
	rv->buf = (char *) xmalloc(size);
	rv->len = size;
	rv->offset = 0;
	return rv;
}

void rmreadbuf(struct readbuffer *buf)
{
	free(buf->buf);
	free(buf);
}

size_t readcall(char *data, size_t size, size_t nmemb, void *udata)
{
	struct readbuffer *rbuf; rbuf = (struct readbuffer *) udata;
	if((rbuf->offset + size * nmemb)>rbuf->len)
	{
		rbuf->buf = xrealloc(rbuf->buf, 2*(rbuf->offset + size * nmemb));
		bzero(rbuf->buf + rbuf->offset + size * nmemb, rbuf->offset + size * nmemb);
	}
	memcpy(rbuf->buf + rbuf->offset, data, size * nmemb);
	rbuf->offset += size * nmemb;
	
	return size * nmemb;
}
