#ifndef SOCKET_H
#define SOCKET_H

#define WGSCK_TCP 0
#define WGSCK_UDP 1

#include <sys/types.h>
#include <sys/socket.h>

#define NI_MAXHOST 1025

struct destinatum
{
	char *host;
	int port;
	int proto;
};

int server_bind(int);
void server_listen(int, int);
int server_accept(int);
int server_write(int, char *, int);
int server_read(int, char *, int *);
void server_close(int);
int server_canread(int);
#endif
