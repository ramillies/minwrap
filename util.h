#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <time.h>

#include "lcfg_static.h"

#define SAY(...) fprintf(stderr, __VA_ARGS__)
#define DEBUG(...) if(conf->logging>1) SAY(__VA_ARGS__)
#define VERBOSE(...) if(conf->logging>0) SAY(__VA_ARGS__)

#define CHOMP(x) (*((x) + strlen(x) - 1) = 0)

void bzero(void *, size_t);


void parser_die(const char *, struct lcfg *);
void die(const char *, ...);
void *xmalloc(size_t);
void *xrealloc(void *, size_t);
int isdir(const char *);
int exists(const char *);
void daemonize(void);
char *hexdump(char *, int);
char *textdump(char *, int);
time_t decode_zone(char *);
time_t current_zone(void);
#endif
