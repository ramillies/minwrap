#ifndef CURLIO_H
#define CURLIO_H

#include <stdlib.h>

#define RESET_HEADERS curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, NULL); curl_easy_setopt(curl, CURLOPT_HEADERDATA, NULL);
#define RESET_DATA curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL); curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);

#define READ_HEADERS(buf) RESET_DATA curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, readcall); curl_easy_setopt(curl, CURLOPT_HEADERDATA, buf);
#define READ_DATA(buf) RESET_HEADERS curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, readcall); curl_easy_setopt(curl, CURLOPT_WRITEDATA, buf);

#define IGNORE_ALL RESET_HEADERS RESET_DATA

struct readbuffer
{
	char *buf;
	size_t len, offset;
};

struct readbuffer *mkreadbuf(size_t);
void rmreadbuf(struct readbuffer *);
size_t readcall(char *, size_t, size_t, void *);

#endif
