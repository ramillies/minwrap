#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <curl/curl.h>

#include "config.h"
#include "util.h"
#include "mail.h"
#include "curlio.h"
#include "pop3.h"
#include "message.h"

CURL *curl;

void curldie(const char *msg, CURLcode result)
{
	fprintf(stderr, "(curl) %s: %s\n", msg, curl_easy_strerror(result));
	exit(1);
}

void initrecv(void)
{
	if(!(curl = curl_easy_init()))
		die("Cannot initialize curl.\n");
	
	char *url;
	url = (char *) xmalloc(400);
	snprintf(url, 400, "%s://%s", PROTOSTRING(conf->proto), conf->mailserver);
	DEBUG("curl uses URL '%s' for initrecv\n", url);
	
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_USERNAME, conf->user);
	curl_easy_setopt(curl, CURLOPT_PASSWORD, conf->pass);

//	if(conf->logging>1) curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

	NCREQUEST("STAT");

	CURLcode result = curl_easy_perform(curl);
	if(result != CURLE_OK)
		curldie("Ingoing mail test failed (check your username, password and mailserver settings)", result);
	makeregex();
	
}

void testsmtp(void)
{
	CURL *test;

	if(!(test = curl_easy_init()))
		die("Cannot initialize libcurl.\n");
	
	char *url;
	url = (char *) xmalloc(400);
	snprintf(url, 400, "smtp://%s", conf->smtpserver);
	DEBUG("curl uses URL '%s' for testsmtp\n", url);

	curl_easy_setopt(test, CURLOPT_URL, url);
	curl_easy_setopt(test, CURLOPT_USERNAME, conf->user);
	curl_easy_setopt(test, CURLOPT_PASSWORD, conf->pass);

	if(conf->logging>1) curl_easy_setopt(test, CURLOPT_VERBOSE, 1);

	curl_easy_setopt(test, CURLOPT_CUSTOMREQUEST, "NOOP");

	CURLcode result = curl_easy_perform(test);
	if(result != CURLE_OK)
		curldie("Outgoing mail test failed (check your username, password and mailserver settings)", result);
	
	curl_easy_cleanup(test);
}

void destroy_recv(void)
{
	curl_easy_cleanup(curl);
}

void checkpop(void)
{
	char *lpname; lpname = (char *) xmalloc(400);
	snprintf(lpname, 400, "%s/last_pull", conf->programdir);
	time_t lastpop = 0;
	if(exists(lpname))
	{
		FILE *f;
		if(f = fopen(lpname, "r"))
		{
			fscanf(f, "%d", &lastpop);
			fclose(f);
		}
	}

	struct numsize *stat; stat = pop3_stat();
	struct numsize *list; list = pop3_listall();

	char *message;
	DEBUG("stat: %d, %d\n", stat->num, stat->size);
	for(int i = 0; i<stat->num; i++)
		DEBUG("\t%d => %d\n", list[i].num, list[i].size);
	for(int i = stat->num-1; i>-1; i--)
	{
		DEBUG("Begin next message\n");
		if(list[i].size < conf->pop_minsize)
			continue;
		message = pop3_headers(list[i].num);
		time_t recvtime = received_time(message);
		char date[80];
		struct tm *time = localtime(&recvtime);
		strftime(date, 80, "%d %b %Y %H:%M:%S", time);
		DEBUG("Message %d: { size = %d Bytes; received at %s }:\n", list[i].num, list[i].size, date);

		if(received_time(message) < lastpop)
		{
			free(message);
			break;
		}
		free(message);

		message = pop3_retr(list[i].num);
		if(handle_message(message) && !conf->mercy)
			pop3_dele(list[i].num);
			
		free(message);
	}

	FILE *f;
	if(!(f = fopen(lpname, "w")))
		die("Cannot save 'last pull' file: %s.\n", strerror(errno));
	fprintf(f, "%d\n", time(NULL) - current_zone());
	fclose(f);

}
