#ifndef CONFIG_H
#define CONFIG_H

#include "lcfg_static.h"

#define POP3 1
#define IMAP 2
#define PROTOSTRING(n) (((n)==1) ? "POP3" : ((n)==2) ? "IMAP" : "unknown")

extern struct WConfig *conf;

struct WConfig
{
	char *emailin, *emailout, *mailserver, *smtpserver, *user, *pass, *programdir, *from;
	int proto, logging, mercy, sleep, port;
	size_t pop_minsize;
};

void init_config(int, char *);
void fill_config(struct lcfg *);

#endif
