#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <poll.h>

#include "util.h"
#include "config.h"
#include "server_socket.h"

/* ***
 *    This is a nice wraparound for send(). It's here basically to ensure that everything gets transferred.
 *    @param sockfd:	A valid filedescriptor of the socket you want to write in.
 *    @param buffer:	A buffer in which the data for sending reside.
 *    @param bufferlen:	Length of data sent.
 *    @return:		0 on success, -1 on error.
*/
int server_write(int sockfd, char *buffer, int bufferlen)
{
	int error = send(sockfd, buffer, bufferlen, 0);
	if(error == 0 || error == bufferlen)
		return 0;
	else if(error == -1)
		return -1;
	else
		// error == number of bytes written, we shall call again
		send(sockfd, buffer + error, bufferlen - error, 0);
}

/* ***
 *    This is another nice wraparound, this time for recv().
 *    @param sockfd:	A valid filedescriptor of the socket you want to read from.
 *    @param buffer:	A buffer in which the read data are to be stored.
 *    @param bufferlen:	How much data we should receive. The length actually received is stored
 *    			into this after the transfer.
 *    @return:		0 for successful transfer, 1 if the other side closed connection.
 *    			If the other side closed, no data were transferred.
 *    			If an error occurs, -1.
*/

int server_read(int sockfd, char *buffer, int *bufferlen)
{
	if(*bufferlen == 0)
		return 0;

	int error = recv(sockfd, buffer, *bufferlen, 0);
	if(error == 0)
		return 1;
	else if(error == -1)
		die("Cannot receive: %s\n", strerror(errno));
	else
		*bufferlen = error;
		return 0;
}

/* ***
 *    Closes the socket. It's only an alias for close().
 *    @param sockfd:	The socket to be closed.
 *    This function does not return anything, nor does it die due to errors.
*/
void server_close(int sockfd)
{
	close(sockfd);
}

/* ***
 *    A wrapper for bind(), this function specifies what's on your end of the connection. Used mostly for
 *    specifying the port which is to be listened on when acting like a server. Socket is bound with REUSEADDR,
 *    so it does not hog ports unreasonably.
 *    @param dest:	A destinatum in which the near end of connection is specified. If the host is NULL,
 *    			address of the local host is filled in automatically.
 *    @return:		A filedescriptor for newly created and bound socket or -1 for an error.
*/
int server_bind(int portnumber)
{
	struct addrinfo hints;
	struct addrinfo *results;
	int error;

	char port[6];
	snprintf(port, 6, "%d", portnumber);

	memset(&hints, 0, sizeof(hints));
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if(error = getaddrinfo(NULL, port, &hints, &results))
	{
		if(
			error == EAI_AGAIN ||
			error == EAI_FAIL ||
			error == EAI_FAMILY ||
			error == EAI_MEMORY ||
			error == EAI_SOCKTYPE
		)
			die("Cannot getaddrinfo while binding: %s\n", gai_strerror(error));
		else return -1;
	}
	
	for(; results != NULL; results = results->ai_next)
	{
		int sockfd;

		sockfd = socket(results->ai_family, results->ai_socktype, results->ai_protocol);
		if(sockfd == -1)
		{
			VERBOSE("Socket creation failed (%s), using next GAI record.", strerror(errno));
			continue;
		}

		int really_yes = 1;
		if((error = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &really_yes, sizeof(int))) == -1)
			die("setsockopt failed while binding: %s", strerror(errno));

		if((error = bind(sockfd, results->ai_addr, results->ai_addrlen)) == -1)
			VERBOSE("Cannot bind (%s), using next GAI record.\n", strerror(errno));
		
		return sockfd;
	}
	return -1;
}

/* ***
 *    An alias for listen(), provides only the name compatibility.
 *    Listen sets the size of the backlog queue for given socket.
 *    @param sockfd:	Which socket is to be modified.
 *    @param backlog:	Size of the backlog queue.
 *    This function does not return anything. If it comes upon an error, it dies.
*/
void server_listen(int sockfd, int backlog)
{
	if(listen(sockfd, backlog) == -1)
		die("listen failed: %s\n", strerror(errno));
}

/* ***
 *    Wrapper for accept() which is kinder because it automatically dies when errors occur and
 *    gives you already politened name of the other side instead of nasty bunch of numbers.
 *    Accept waits for someone to connect to a bound and listening socket. When someone connects,
 *    it makes a new socket with new FD and returns it. All communication with the connected client
 *    is carried out via this new socket.
 *    @param sockfd:	The socket which listens for connections.
 *    @param who:	The pointer to the string in which the polite name is to be placed.
 *    @param len:	The pointer in which the length of the name will reside.
 *    @return:		File descriptor of the "private" socket or -1 on error.
*/
int server_accept(int sockfd)
{
	struct sockaddr_storage other_end;
	int magnitude = sizeof(struct sockaddr_storage);
	int privatefd = accept(sockfd, (struct sockaddr *) &other_end, &magnitude);
	if(privatefd == -1)
		return -1;

	return privatefd;
}

int server_canread(int sockfd)
{
	struct pollfd desc = { .fd = sockfd, .events = POLLIN, .revents = 0 };
	int result;
	switch( result = poll(&desc, 1, 200) )
	{
		case 1:
			return 1;
		case -1:
			if(errno != EINTR)
				die("Cannot poll: %s\n", strerror(errno));
		case 0:
			return 0;
	}
}
