#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>

#include "config.h"
#include "util.h"
#include "mail.h"
#include "curlio.h"
#include "pop3.h"

struct numsize *pop3_stat(void)
{
	struct numsize *rt; rt = (struct numsize *) xmalloc(sizeof(struct numsize));
	struct readbuffer *buf;
	buf = mkreadbuf(20);

	NCREQUEST("STAT");
	READ_HEADERS(buf);
	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while POP3 STAT", resp);
	
	sscanf(buf->buf, "+OK %d %ld", &(rt->num), &(rt->size));

	rmreadbuf(buf);
	return rt;
}

struct numsize *pop3_list(int num)
{
	struct numsize *rt; rt = (struct numsize *) xmalloc(sizeof(struct numsize));
	struct readbuffer *buf;
	buf = mkreadbuf(20);

	char request[20];
	snprintf(request, 30, "LIST %d", num);

	NCREQUEST(request);
	READ_HEADERS(buf);
	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while POP3 LIST (one)", resp);
	
	sscanf(buf->buf, "+OK %d %ld", &(rt->num), &(rt->size));

	rmreadbuf(buf);
	return rt;
}

void pop3_dele(int num)
{
	char request[20];
	snprintf(request, 30, "DELE %d", num);

	NCREQUEST(request);
	IGNORE_ALL;
	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while POP3 DELE (one)", resp);
}

struct numsize *pop3_listall(void)
{
	struct numsize *stat; stat = pop3_stat();
	struct numsize *list; list = (struct numsize *) xmalloc(stat->num * sizeof(struct numsize));

	struct readbuffer *buf; buf = mkreadbuf(512);

	REQUEST("LIST");
	READ_DATA(buf);
	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while POP3 LIST (all)", resp);
	
	DEBUG("DATA: %s", buf->buf);
	for(int i = 0; i<stat->num && buf->buf != NULL; i++, buf->buf = strchr(buf->buf+1, '\n'))
	{
		sscanf(buf->buf, "%d %ld", &(list[i].num), &(list[i].size));
		DEBUG("Found %d, %d\n", list[i].num, list[i].size);
	}
	return list;
}

char *pop3_retr(int num)
{
	struct numsize *size;
	size = pop3_list(num);

	struct readbuffer *buf; buf = mkreadbuf(size->size+3);
	char request[30]; snprintf(request, 30, "RETR %d", num);
	REQUEST(request);
	READ_DATA(buf);

	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while retrieving message", resp);
	
	return buf->buf;
}

char *pop3_headers(int num)
{
	struct numsize *size;
	size = pop3_list(num);

	struct readbuffer *buf; buf = mkreadbuf(512);
	char request[30]; snprintf(request, 30, "TOP %d 0", num);
	REQUEST(request);
	READ_DATA(buf);

	CURLcode resp;
	if((resp = curl_easy_perform(curl)) != CURLE_OK)
		curldie("Error while retrieving message headers", resp);
	
	return buf->buf;
}
